using Microsoft.AspNetCore.Mvc;

namespace REST.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class BaseController: Controller
    {
        
    }
}