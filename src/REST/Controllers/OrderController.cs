using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace REST.Controllers
{
    public class OrderController : BaseController
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        #region Get

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            IReadOnlyCollection<OrderDTO> orders = await _orderService.GetAll();
            return new ObjectResult(orders);
        }


        [HttpGet("{id:int}", Name = "GetOrder")]
        public async Task<IActionResult> GetById(int id)
        {
            OrderDTO order = await _orderService.GetById(id);
            return order == null
                ? NotFound($"Отсутствует заказ по заданному id:{id}")
                : new ObjectResult(order);
        }

        #endregion

        #region Post

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] OrderDTO orderDto)
        {
            if (orderDto == null) return BadRequest(ModelState);
            try
            {
                if (ModelState.IsValid)
                {
                    OrderDTO newOrder = await _orderService.Create(orderDto);
                    return CreatedAtRoute("GetOrder", new {id = newOrder.OrderId}, newOrder);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion

        #region PUT

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, [FromBody] OrderDTO orderDto)
        {
            if (orderDto == null) return BadRequest(ModelState);
            try
            {
                if (ModelState.IsValid)
                {
                    OrderDTO updateOrder =
                        await _orderService.Update(id, orderDto);
                    return CreatedAtRoute("GetItem",
                        new {id = updateOrder.OrderId}, updateOrder);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion

        #region Delete

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                OrderDTO orderDto = await _orderService.Delete(id);
                return orderDto == null
                    ? (IActionResult) NotFound($"Отсутствует вид доставки по заданному id:{id}")
                    : NoContent();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion
    }
}