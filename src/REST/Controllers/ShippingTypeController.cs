using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace REST.Controllers
{
    public class ShippingTypeController : BaseController
    {
        private readonly IShippingTypeService _shippingTypeService;

        public ShippingTypeController(IShippingTypeService shippingTypeService)
        {
            _shippingTypeService = shippingTypeService;
        }

        #region Get

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            IReadOnlyCollection<ShippingTypeDTO> shippingTypes = await _shippingTypeService.GetAll();
            return new ObjectResult(shippingTypes);
        }

        [HttpGet("{id:int}", Name = "GetShippingType")]
        public async Task<IActionResult> GetById(int id)
        {
            ShippingTypeDTO shippingTypeDto = await _shippingTypeService.GetById(id);
            return shippingTypeDto == null
                ? NotFound($"Отсутствует вид доставки по заданному id:{id}")
                : new ObjectResult(shippingTypeDto);
        }

        #endregion

        #region Post

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] ShippingTypeDTO shippingTypeDto)
        {
            if (shippingTypeDto == null) return BadRequest(ModelState);
            try
            {
                if (ModelState.IsValid)
                {
                    ShippingTypeDTO newShippingType = await _shippingTypeService.Create(shippingTypeDto);
                    return CreatedAtRoute("GetShippingType", new {id = newShippingType.ShippingTypeId},
                        newShippingType);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion

        #region PUT

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, [FromBody] ShippingTypeDTO shippingTypeDto)
        {
            if (shippingTypeDto == null) return BadRequest(ModelState);
            try
            {
                if (ModelState.IsValid)
                {
                    ShippingTypeDTO updateShippingType =
                        await _shippingTypeService.Update(id, shippingTypeDto);
                    return CreatedAtRoute("GetCategory",
                        new {id = updateShippingType.ShippingTypeId}, updateShippingType);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion

        #region Delete

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                ShippingTypeDTO shippingTypeDto = await _shippingTypeService.Delete(id);
                return shippingTypeDto == null
                    ? (IActionResult) NotFound($"Отсутствует вид доставки по заданному id:{id}")
                    : NoContent();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion
    }
}