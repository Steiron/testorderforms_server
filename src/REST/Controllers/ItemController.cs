using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace REST.Controllers
{
    public class ItemController : BaseController
    {
        private readonly IItemService _itemService;

        public ItemController(IItemService itemService)
        {
            _itemService = itemService;
        }

        #region Get

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            IReadOnlyCollection<ItemDTO> items = await _itemService.GetAll();
            return new ObjectResult(items);
        }


        [HttpGet("{id:int}", Name = "GetItem")]
        public async Task<IActionResult> GetById(int id)
        {
            ItemDTO itemDto = await _itemService.GetById(id);
            return itemDto == null
                ? NotFound($"Отсутствует товар по заданному id:{id}")
                : new ObjectResult(itemDto);
        }

        #endregion

        #region Post

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] ItemDTO itemDto)
        {
            if (itemDto == null) return BadRequest(ModelState);
            try
            {
                if (ModelState.IsValid)
                {
                    ItemDTO newItem = await _itemService.Create(itemDto);
                    return CreatedAtRoute("GetItem", new {id = newItem.ItemId}, newItem);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion

        #region PUT

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, [FromBody] ItemDTO itemDto)
        {
            if (itemDto == null) return BadRequest(ModelState);
            try
            {
                if (ModelState.IsValid)
                {
                    ItemDTO updateItem =
                        await _itemService.Update(id, itemDto);
                    return CreatedAtRoute("GetItem",
                        new {id = updateItem.ItemId}, updateItem);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion

        #region Delete

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                ItemDTO itemDto = await _itemService.Delete(id);
                return itemDto == null
                    ? (IActionResult) NotFound($"Отсутствует вид доставки по заданному id:{id}")
                    : NoContent();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion
    }
}