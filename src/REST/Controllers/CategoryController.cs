using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace REST.Controllers
{
    public class CategoryController : BaseController
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        #region Get

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            IReadOnlyCollection<CategoryDTO> categoriesDto = await _categoryService.GetAll();
            return new ObjectResult(categoriesDto);
        }

        [HttpGet("{id:int}", Name = "GetCategory")]
        public async Task<IActionResult> GetById(int id)
        {
            CategoryDTO categoryDto = await _categoryService.GetById(id);
            return categoryDto == null
                ? NotFound($"Отсутствует категория по заданному id:{id}")
                : new ObjectResult(categoryDto);
        }

        #endregion

        #region Post

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CategoryDTO categoryDto)
        {
            if (categoryDto == null) return BadRequest(ModelState);
            try
            {
                if (ModelState.IsValid)
                {
                    CategoryDTO newCategory = await _categoryService.Create(categoryDto);
                    return CreatedAtRoute("GetCategory", new {id = newCategory.CategoryID}, newCategory);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion

        #region PUT

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, [FromBody] CategoryDTO categoryDto)
        {
            if (categoryDto == null) return BadRequest(ModelState);
            try
            {
                if (ModelState.IsValid)
                {
                    CategoryDTO updateCategory = await _categoryService.Update(id, categoryDto);
                    return CreatedAtRoute("GetCategory", new {id = updateCategory.CategoryID}, updateCategory);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion

        #region Delete

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                CategoryDTO categoryDto = await _categoryService.Delete(id);
                return categoryDto == null
                    ? (IActionResult) NotFound($"Отсутствует категория по заданному id:{id}")
                    : NoContent();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion
    }
}