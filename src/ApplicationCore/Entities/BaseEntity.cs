namespace ApplicationCore.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; private set; }

        public abstract void Update(BaseEntity entity);
    }
}