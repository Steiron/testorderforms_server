namespace ApplicationCore.Entities
{
    public class Order : BaseEntity
    {
        public string Name { get; private set; }
        public Item Item { get; private set; }
        public bool IsShipping { get; private set; }
        public double? OrderDate { get; private set; }
        public double? ShippingDate { get; private set; }
        public ShippingType ShippingType { get; private set; }

        public int ItemId { get; private set; }
        public int ShippingTypeId { get; private set; }

        private Order()
        {
        }


        public Order(string name, 
            int itemId, int shippingTypeId, bool isShipping = false,
            double? orderDate = null, double? shippingDate = null)
        {
            Update(name, isShipping, orderDate, shippingDate, itemId, shippingTypeId);
        }

        public void Update(string name, bool isShipping,
            double? orderDate, double? shippingDate,
            int itemId, int shippingTypeId)
        {
            Name = name;
            IsShipping = isShipping;
            OrderDate = orderDate;
            ShippingDate = shippingDate;
            ItemId = itemId;
            ShippingTypeId = shippingTypeId;
        }

        public override void Update(BaseEntity entity)
        {
            Order order = (Order) entity;
            Update(order.Name, order.IsShipping,
                order.OrderDate, order.ShippingDate,
                order.ItemId, order.ShippingTypeId);
        }
    }
}