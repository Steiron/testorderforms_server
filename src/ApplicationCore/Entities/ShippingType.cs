namespace ApplicationCore.Entities
{
    public class ShippingType : BaseEntity
    {
        public string Name { get; private set; }

        public ShippingType(string name)
        {
            Name = name;
        }

        public void Update(string name)
        {
            Name = name;
        }

        public override void Update(BaseEntity entity)
        {
            ShippingType shippingType = ((ShippingType) entity);
            Update(shippingType.Name);
        }
    }
}