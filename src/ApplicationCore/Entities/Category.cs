using System.ComponentModel.DataAnnotations;

namespace ApplicationCore.Entities
{
    public class Category : BaseEntity
    {
        public string Name { get; private set; }

        private Category()
        {
        }


        public Category(string name) => Update(name);


        public void Update(string name)
        {
            Name = name;
        }

        public override void Update(BaseEntity entity)
        {
            Category category = ((Category) entity);
            Update(category.Name);
        }
    }
}