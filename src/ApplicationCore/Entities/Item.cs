using Newtonsoft.Json;

namespace ApplicationCore.Entities
{
    public class Item : BaseEntity
    {
        public string Name { get; private set; }
        public Category Category { get; private set; }
        public double Price { get; private set; }
        public int CategoryID { get; private set; }

        private Item()
        {
        }

        public Item(string name, double price, int categoryId)
        {
            Name = name;
            Price = price;
            CategoryID = categoryId;
        }

        public void Update(string name, double price, int categoryId)
        {
            Name = name;
            Price = price;
            CategoryID = categoryId;
        }

        public override void Update(BaseEntity entity)
        {
            Item item = ((Item) entity);
            Update(item.Name, item.Price, item.CategoryID);
        }
    }
}