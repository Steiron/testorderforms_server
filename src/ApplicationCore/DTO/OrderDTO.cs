using ApplicationCore.Entities;
using Newtonsoft.Json;

namespace ApplicationCore.DTO
{
    public class OrderDTO : BaseEntityDto<Order>
    {
        public int OrderId { get; private set; }
        public string Name { get; private set; }

        [JsonProperty("Item")]
        public ItemDTO ItemDto { get; private set; }

        public bool IsShipping { get; private set; }
        public double? OrderDate { get; private set; }
        public double? ShippingDate { get; private set; }

        [JsonProperty("ShippingType")]
        public ShippingTypeDTO ShippingType { get; private set; }

        [JsonProperty("ItemId")]
        public int ItemDtoId { get; private set; }

        [JsonProperty("ShippingTypeId")]
        public int ShippingTypeDtoId { get; private set; }

        private OrderDTO()
        {
        }

        public OrderDTO(string name, bool isShipping, double? orderDate, double? shippingDate, int itemDtoId,
            int shippingTypeDtoId)
        {
            Name = name;
            this.IsShipping = isShipping;
            OrderDate = orderDate;
            ShippingDate = shippingDate;
            ItemDtoId = itemDtoId;
            ShippingTypeDtoId = shippingTypeDtoId;
        }

        public override Order ConvertToEntity()
        {
            return new Order(Name, ItemDtoId, ShippingTypeDtoId, IsShipping, OrderDate, ShippingDate);
        }
    }
}