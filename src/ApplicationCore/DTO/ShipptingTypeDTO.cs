using System.ComponentModel.DataAnnotations;
using ApplicationCore.Entities;

namespace ApplicationCore.DTO
{
    public class ShippingTypeDTO : BaseEntityDto<ShippingType>
    {
        [Required]
        public int ShippingTypeId { get; private set; }

        [Required]
        [MaxLength(15)]
        public string Name { get; private set; }


        public ShippingTypeDTO(string name)
        {
            Name = name;
        }

        public override ShippingType ConvertToEntity()
        {
            return new ShippingType(Name);
        }
    }
}