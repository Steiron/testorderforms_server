using ApplicationCore.Entities;
using Newtonsoft.Json;

namespace ApplicationCore.DTO
{
    public class ItemDTO : BaseEntityDto<Item>
    {
        public int ItemId { get; private set; }
        public string Name { get; private set; }

        [JsonProperty("Category")]
        public CategoryDTO CategoryDto { get; private set; }

        public double Price { get; private set; }

        [JsonProperty("CategoryId")]
        public int CategoryDtoId { get; private set; }

        private ItemDTO()
        {
        }

        public ItemDTO(string name, double price, int categoryDtoId)
        {
            Name = name;
            Price = price;
            CategoryDtoId = categoryDtoId;
        }

        public override Item ConvertToEntity()
        {
            return new Item(Name, Price, CategoryDtoId);
        }
    }
}