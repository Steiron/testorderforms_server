using System.ComponentModel.DataAnnotations;
using ApplicationCore.Entities;

namespace ApplicationCore.DTO
{
    public class CategoryDTO : BaseEntityDto<Category>
    {
        [Required]
        public int CategoryID { get; private set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; private set; }


        private CategoryDTO()
        {
        }

        public CategoryDTO(string name)
        {
            Name = name;
        }

        public override Category ConvertToEntity()
        {
            return new Category(Name);
        }
    }
}