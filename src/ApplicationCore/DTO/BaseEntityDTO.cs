using ApplicationCore.Entities;

namespace ApplicationCore.DTO
{
    public abstract class BaseEntityDto<TEntity> where  TEntity : BaseEntity
    {
        public abstract TEntity ConvertToEntity();
    }
}