using ApplicationCore.DTO;
using ApplicationCore.Entities;
using AutoMapper;

namespace ApplicationCore.Mappers
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<Category, CategoryDTO>()
                .ForMember(f => f.CategoryID,
                    to => to.MapFrom(m => m.Id));


            CreateMap<ShippingType, ShippingTypeDTO>()
                .ForMember(f => f.ShippingTypeId,
                    to => to.MapFrom(m => m.Id));

            CreateMap<Item, ItemDTO>()
                .ForMember(f => f.ItemId,
                    to => to.MapFrom(m => m.Id))
                .ForMember(f => f.CategoryDtoId,
                    to => to.MapFrom(m => m.CategoryID))
                .ForMember(f => f.CategoryDto,
                    to => to.MapFrom(m => m.Category));

            CreateMap<Order, OrderDTO>()
                .ForMember(f => f.OrderId,
                    to => to.MapFrom(m => m.Id))
                .ForMember(f => f.ItemDtoId,
                    to => to.MapFrom(m => m.ItemId))
                .ForMember(f => f.ShippingTypeDtoId,
                    to => to.MapFrom(m => m.ShippingTypeId))
                .ForMember(f => f.ItemDto,
                    to => to.MapFrom(m => m.Item))
                /*.ForMember(f => f.ShippingTypeDto,
                    to => to.MapFrom(m => m.ShippingType))*/;
        }
    }
}