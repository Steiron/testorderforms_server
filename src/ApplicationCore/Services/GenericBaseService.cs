using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repository;
using ApplicationCore.Interfaces.Services;
using AutoMapper;

namespace ApplicationCore.Services
{
    public class GenericBaseService<TEntityDto, TEntity> : IGenericBaseService<TEntityDto, TEntity>
        where TEntityDto : BaseEntityDto<TEntity>
        where TEntity : BaseEntity
    {
        #region Properties

        protected readonly IGenericRepository<TEntity> Repository;
        protected readonly IMapper Mapper;

        #endregion

        #region Construcor

        public GenericBaseService(IGenericRepository<TEntity> repository, IMapper mapper)
        {
            Repository = repository;
            Mapper = mapper;
        }

        #endregion

        #region Get

        public async Task<IReadOnlyCollection<TEntityDto>> GetAll()
        {
            IReadOnlyCollection<TEntity> entities = await Repository.GetAllAsync();
            return Mapper.Map<IReadOnlyCollection<TEntityDto>>(entities);
        }

        public async Task<TEntityDto> GetById(int id)
        {
            TEntity entity = await Repository.GetByIdAsync(id);
            return Mapper.Map<TEntityDto>(entity);
        }

        #endregion

        #region Create

        public async Task<TEntityDto> Create(TEntityDto itemDto)
        {
            TEntity entity = itemDto.ConvertToEntity();

            await Repository.AddAsync(entity);
            await Repository.Save();

            return Mapper.Map<TEntityDto>(entity);
        }

        #endregion

        #region Update

        public async Task<TEntityDto> Update(int id, TEntityDto entityDto)
        {
            TEntity entity = await Repository.GetByIdAsync(id);
            if (entity == null) return null;

            TEntity convertEntity = entityDto.ConvertToEntity();

            entity.Update(convertEntity);
            entity = Repository.Update(entity);
            await Repository.Save();

            return Mapper.Map<TEntityDto>(entity);
        }

        #endregion

        #region Delete

        public async Task<TEntityDto> Delete(int id)
        {
            TEntity entity = await Repository.GetByIdAsync(id);
            if (entity == null) return null;

            Repository.Delete(entity);
            await Repository.Save();

            return Mapper.Map<TEntityDto>(entity);
        }

        #endregion
    }
}