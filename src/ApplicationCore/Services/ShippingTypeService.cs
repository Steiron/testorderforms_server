using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repository;
using ApplicationCore.Interfaces.Services;
using AutoMapper;

namespace ApplicationCore.Services
{
    public class ShippingTypeService : GenericBaseService<ShippingTypeDTO, ShippingType>, IShippingTypeService
    {
        #region Constructor

        #endregion

        public ShippingTypeService(IGenericRepository<ShippingType> repository, IMapper mapper)
            : base(repository, mapper)
        {
        }
    }
}