using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repository;
using ApplicationCore.Interfaces.Services;
using AutoMapper;

namespace ApplicationCore.Services
{
    public class CategoryService : GenericBaseService<CategoryDTO, Category>, ICategoryService
    {
        #region Constructor

        public CategoryService(IGenericRepository<Category> repository, IMapper mapper) : base(repository, mapper)
        {
        }

        #endregion
    }
}