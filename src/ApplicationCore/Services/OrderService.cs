using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repository;
using ApplicationCore.Interfaces.Services;
using AutoMapper;

namespace ApplicationCore.Services
{
    public class OrderService : GenericBaseService<OrderDTO, Order>, IOrderService
    {
        #region Constructor

        public OrderService(IOrderRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }

        #endregion
    }
}