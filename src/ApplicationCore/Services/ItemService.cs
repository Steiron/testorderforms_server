using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repository;
using ApplicationCore.Interfaces.Services;
using AutoMapper;

namespace ApplicationCore.Services
{
    public class ItemService : GenericBaseService<ItemDTO, Item>, IItemService
    {
        private IItemRepository _itemRepository;

        public ItemService(IItemRepository repository, IMapper mapper) : base(repository, mapper)
        {
            _itemRepository = repository;
        }


        public async Task<IReadOnlyCollection<ItemDTO>> GetItemsByCategoryId(int categoryId)
        {
            IReadOnlyCollection<Item> items = await _itemRepository.GetItemsByCategoryId(categoryId);
            return Mapper.Map<IReadOnlyCollection<ItemDTO>>(items);
        }
    }
}