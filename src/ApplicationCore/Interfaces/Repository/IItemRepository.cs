using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.Repository
{
    public interface IItemRepository : IGenericRepository<Item>
    {
        Task<IReadOnlyCollection<Item>> GetItemsByCategoryId(int categoryId);
        
    }
}