using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.Repository
{
    public interface IOrderRepository : IGenericRepository<Order>
    {
    }
}