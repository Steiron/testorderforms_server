using System.Threading.Tasks;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.Repository
{
    public interface IGenericRepository<TEntity> : IAsyncRepository<TEntity> where TEntity : BaseEntity
    {
        Task<int> Save();
        TEntity Update(TEntity entity);
        TEntity Delete(TEntity entity);
    }
}