using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repository;

namespace ApplicationCore.Interfaces.Services
{
    public interface IGenericBaseService<TEntityDto, TEntity>
        where TEntityDto : BaseEntityDto<TEntity>
        where TEntity : BaseEntity
    {
        Task<IReadOnlyCollection<TEntityDto>> GetAll();
        Task<TEntityDto> GetById(int id);
        Task<TEntityDto> Create(TEntityDto entityDto);
        Task<TEntityDto> Update(int id, TEntityDto entityDto);
        Task<TEntityDto> Delete(int id);
    }
}