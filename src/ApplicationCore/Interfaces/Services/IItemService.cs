using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.Services
{
    public interface IItemService : IGenericBaseService<ItemDTO, Item>
    {
        Task<IReadOnlyCollection<ItemDTO>> GetItemsByCategoryId(int categoryId);
    }
}