using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.Services
{
    public interface ICategoryService : IGenericBaseService<CategoryDTO, Category>
    {
       
    }
}