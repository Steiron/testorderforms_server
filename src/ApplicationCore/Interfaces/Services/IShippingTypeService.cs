using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.Services
{
    public interface IShippingTypeService : IGenericBaseService<ShippingTypeDTO, ShippingType>
    {
    }
}