using ApplicationCore.DTO;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.Services
{
    public interface IOrderService : IGenericBaseService<OrderDTO, Order>
    {
    }
}