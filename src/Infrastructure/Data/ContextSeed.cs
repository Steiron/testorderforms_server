using System.Linq;
using System.Threading.Tasks;
using Infrastructure.Data.Seeds;

namespace Infrastructure.Data
{
    public class ContextSeed
    {
        public static async Task SeedAsync(OrderContext context)
        {
            if (!context.ShippingTypes.Any())
            {
                await context.ShippingTypes.AddRangeAsync(ShippingTypsSeed.Get());
                await context.SaveChangesAsync();
            }

            if (!context.Categories.Any())
            {
                await context.Categories.AddRangeAsync(CategorySeed.Get());
                await context.SaveChangesAsync();
            }

            if (!context.Items.Any())
            {
                await context.Items.AddRangeAsync(ItemSeed.Get());
                await context.SaveChangesAsync();
            }

            if (!context.Orders.Any())
            {
                await context.Orders.AddRangeAsync(OrderSeed.Get());
                await context.SaveChangesAsync();
            }
        }
    }
}