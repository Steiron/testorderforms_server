using System.Collections.Generic;
using ApplicationCore.Entities;

namespace Infrastructure.Data.Seeds
{
    public class ItemSeed
    {
        public static IReadOnlyCollection<Item> Get()
        {
            return new[]
            {
                new Item("Рубашка", 20, 1),
                new Item("CLR via C#", 30, 2),
                new Item("Смартфон", 200, 3),
            };
        }
    }
}