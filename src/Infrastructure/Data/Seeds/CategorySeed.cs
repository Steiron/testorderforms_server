using System.Collections.Generic;
using ApplicationCore.Entities;

namespace Infrastructure.Data.Seeds
{
    public class CategorySeed
    {
        public static IReadOnlyCollection<Category> Get()
        {
            return new[]
            {
                new Category("Одежда"),
                new Category("Книги"),
                new Category("Электроника")
            };
        }
    }
}