using System.Collections.Generic;
using ApplicationCore.Entities;

namespace Infrastructure.Data.Seeds
{
    public class OrderSeed
    {
        public static IReadOnlyCollection<Order> Get()
        {
            return new[]
            {
                new Order("Рубашка 2019", 1, 1),
                new Order("Рубашка 2019", 1, 3, true),
                new Order("Samsung 9", 3, 3),
                new Order("Учебник C#", 2, 2, true)
            };
        }
    }
}