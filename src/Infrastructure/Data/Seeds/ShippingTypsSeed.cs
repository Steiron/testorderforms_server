using System.Collections.Generic;
using ApplicationCore.Entities;

namespace Infrastructure.Data.Seeds
{
    public class ShippingTypsSeed
    {
        public static IReadOnlyCollection<ShippingType> Get()
        {
            return new[]
            {
                new ShippingType("Авиа"),
                new ShippingType("Самовывоз"),
                new ShippingType("Курьер"),

            };
        }
    }
}