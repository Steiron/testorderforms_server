using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data.Repository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        protected OrderContext OrderContext;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(OrderContext orderContext)
        {
            OrderContext = orderContext;
            _dbSet = OrderContext.Set<TEntity>();
        }

        public virtual async  Task<TEntity> GetByIdAsync(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public virtual async Task<IReadOnlyCollection<TEntity>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
            return entity;
        }

        public async Task<int> Save()
        {
            return await OrderContext.SaveChangesAsync();
        }

        public TEntity Update(TEntity entity)
        {
            _dbSet.Attach(entity).State = EntityState.Modified;
            return entity;
        }

        public TEntity Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
            return entity;
        }
    }
}