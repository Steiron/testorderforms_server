using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data.Repository
{
    public class ItemRepository : GenericRepository<Item>, IItemRepository
    {
        public ItemRepository(OrderContext orderContext) : base(orderContext)
        {
        }

        public override async Task<Item> GetByIdAsync(int id)
        {
            return await OrderContext.Items
                .Include(i => i.Category)
                .FirstOrDefaultAsync(i => i.Id == id);
        }

        public override async Task<IReadOnlyCollection<Item>> GetAllAsync()
        {
            return await OrderContext.Items
                .Include(i => i.Category)
                .ToListAsync();
        }


        public async Task<IReadOnlyCollection<Item>> GetItemsByCategoryId(int categoryId)
        {
            return await OrderContext.Items
                .Where(i => i.CategoryID == categoryId)
                .ToListAsync();
        }
    }
}