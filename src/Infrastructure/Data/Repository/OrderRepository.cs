using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data.Repository
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(OrderContext orderContext) : base(orderContext)
        {
        }

        public override async Task<IReadOnlyCollection<Order>> GetAllAsync()
        {
            return await OrderContext.Orders
                .Include(o => o.ShippingType)
                .Include(o => o.Item)
                .ThenInclude(i => i.Category)
                .ToListAsync();
        }

        public override async Task<Order> GetByIdAsync(int id)
        {
            return await OrderContext.Orders
                .Include(o => o.ShippingType)
                .Include(o => o.Item)
                .ThenInclude(i => i.Category)
                .FirstOrDefaultAsync(o => o.Id == id);
        }
    }
}