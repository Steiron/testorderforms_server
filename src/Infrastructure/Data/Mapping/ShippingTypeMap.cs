using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.Mapping
{
    public class ShippingTypeMap : IEntityTypeConfiguration<ShippingType>
    {
        public void Configure(EntityTypeBuilder<ShippingType> builder)
        {
            builder.ToTable("ShippingType");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Name)
                .HasMaxLength(15)
                .IsRequired();
        }
    }
}